package com.ts.client;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private TextView mTextViewReceive;
    private EditText mEditTextIP;
    private EditText mEditTextSend;
    private Button mButtonConnect;
    private Button mButtonSend;

    private JWSClient mJWSClient;
    private JWSClient.JWSCallBack mJWSCallBack = new JWSClient.JWSCallBack() {
        @Override
        public void onOpen(ServerHandshake handshakedata) {

        }

        @Override
        public void onMessage(String message) {
            Message msg = Message.obtain();
            msg.what = 0;
            msg.obj = message;
            mUIHandler.sendMessage(msg);
        }

        @Override
        public void onMessage(ByteBuffer bytes) {
            Message msg = Message.obtain();
            msg.what = 0;
            msg.obj = getString(bytes);
            mUIHandler.sendMessage(msg);
        }
    };

    private Handler mUIHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    mTextViewReceive.setText((String) msg.obj);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        mEditTextIP = findViewById(R.id.edit_ip);
        mEditTextSend = findViewById(R.id.edit_send);
        mButtonConnect = findViewById(R.id.btn_connect);
        mButtonSend = findViewById(R.id.btn_send);
        mTextViewReceive = findViewById(R.id.text_receive);
    }

    public void connectServe(View view) {
        String ip = mEditTextIP.getText().toString();
        if (ip != null && ip.startsWith("192.168.")) {
            try {
                mJWSClient = new JWSClient(new URI("ws://" + ip + ":13333"), mJWSCallBack);
                mJWSClient.setConnectionLostTimeout(5);
                mJWSClient.connectBlocking(3, TimeUnit.SECONDS);
            } catch (URISyntaxException | InterruptedException exception) {
                exception.printStackTrace();
                Toast.makeText(MainActivity.this, "连接失败", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(MainActivity.this, "请输入正确IP", Toast.LENGTH_SHORT).show();
        }
    }

    public void send(View view) {
        String msg = mEditTextSend.getText().toString();
        if (msg != null && !"".equals(msg)) {
            if (mJWSClient != null && mJWSClient.isOpen()) {
                Log.d(TAG, "send: ");
                mJWSClient.send(msg);
            }
        } else {
            Toast.makeText(MainActivity.this, "发送不能为空", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 将ByteBuffer转换为String
     *
     * @param buffer
     * @return
     */
    public static String getString(ByteBuffer buffer) {
        Charset charset = null;
        CharsetDecoder decoder = null;
        CharBuffer charBuffer = null;
        try {
            charset = Charset.forName("UTF-8");
            decoder = charset.newDecoder();
            // charBuffer = decoder.decode(buffer);//用这个的话，只能输出来一次结果，第二次显示为空
            charBuffer = decoder.decode(buffer.asReadOnlyBuffer());
            return charBuffer.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mJWSClient != null) {
            mJWSClient.close();
        }
    }
}