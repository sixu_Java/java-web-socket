package com.ts.client;

import android.util.Log;

import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.nio.ByteBuffer;

public class JWSClient extends WebSocketClient {
    private static final String TAG = "JWSClient";
    private JWSCallBack mCallBack;

    public JWSClient(URI serverUri, JWSCallBack callBack) {
        this(serverUri);
        mCallBack = callBack;
    }

    private JWSClient(URI serverUri) {
        super(serverUri);
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        Log.d(TAG, "onOpen: ");
        mCallBack.onOpen(handshakedata);
    }

    @Override
    public void onMessage(String message) {
        Log.d(TAG, "onMessage: ");
        mCallBack.onMessage(message);
    }

    @Override
    public void onMessage(ByteBuffer bytes) {
        super.onMessage(bytes);
        Log.d(TAG, "onMessage: ");
        mCallBack.onMessage(bytes);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        Log.d(TAG, "onClose: code = " + code + ", reason = " + reason + ", remote = " + remote);
        if (code==-1){
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    reconnectBlocking();
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onError(Exception ex) {
        Log.d(TAG, "onError: " + ex.getMessage());
    }

    interface JWSCallBack {
        void onOpen(ServerHandshake handshakedata);

        void onMessage(String message);

        void onMessage(ByteBuffer bytes);
    }

}
