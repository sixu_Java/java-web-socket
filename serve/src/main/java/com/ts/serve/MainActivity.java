package com.ts.serve;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    private TextView mTextViewReceive;
    private TextView mTextConnectClient;
    private JWSServe mJWSServe;
    private List<String> mClientIpList = new ArrayList<>();

    private JWSServe.JWSCallBack mJWSCallBack = new JWSServe.JWSCallBack() {
        @Override
        public void onOpen(ClientHandshake handshake) {

        }

        @Override
        public void onMessage(String message) {
            Message msg = Message.obtain();
            msg.what = 0;
            msg.obj = message;
            mUIHandler.sendMessage(msg);
        }

        @Override
        public void onMessage(ByteBuffer bytes) {
            Message msg = Message.obtain();
            msg.what = 0;
            msg.obj = getString(bytes);
            mUIHandler.sendMessage(msg);
        }

        @Override
        public void updateSocketClient(HashMap<WebSocket, String> socketHashMap) {
            mClientIpList = new ArrayList<>(socketHashMap.values());
            mUIHandler.sendEmptyMessage(1);
        }
    };

    private Handler mUIHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    mTextViewReceive.setText((String) msg.obj);
                    break;
                case 1:
                    StringBuffer stringBuffer = new StringBuffer();
                    if (mClientIpList.size() < 1) {
                        stringBuffer.append("当前无设备连接\n");
                    } else {
                        for (String ip : mClientIpList) {
                            stringBuffer.append(ip + ":已连接\n");
                        }
                    }
                    mTextConnectClient.setText(stringBuffer.toString());
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextViewReceive = findViewById(R.id.text_receive);
        mTextConnectClient = findViewById(R.id.text_connect_client);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mJWSServe = new JWSServe(13333, mJWSCallBack);
        mJWSServe.setConnectionLostTimeout(5);
        mJWSServe.start();
    }

    public void send(View view) {
        String msg = ((EditText) findViewById(R.id.edit_send)).getText().toString();
        if (msg != null && !"".equals(msg)) {
            mJWSServe.send(mClientIpList, msg);
        } else {
            Toast.makeText(MainActivity.this, "发送不能为空", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 将ByteBuffer转换为String
     *
     * @param buffer
     * @return
     */
    public static String getString(ByteBuffer buffer) {
        Charset charset = null;
        CharsetDecoder decoder = null;
        CharBuffer charBuffer = null;
        try {
            charset = Charset.forName("UTF-8");
            decoder = charset.newDecoder();
            // charBuffer = decoder.decode(buffer);//用这个的话，只能输出来一次结果，第二次显示为空
            charBuffer = decoder.decode(buffer.asReadOnlyBuffer());
            return charBuffer.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            mJWSServe.stop();
        } catch (IOException exception) {
            exception.printStackTrace();
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}