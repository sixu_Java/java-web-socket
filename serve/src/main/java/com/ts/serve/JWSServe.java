package com.ts.serve;

import android.util.Log;

import org.java_websocket.WebSocket;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JWSServe extends WebSocketServer {
    private static final String TAG = "JWSServe";

    private JWSCallBack mCallBack;
    private HashMap<WebSocket, String> mSocketClients;

    public JWSServe(int port, JWSCallBack callBack) {
        super(new InetSocketAddress(port));
        mCallBack = callBack;
        mSocketClients = new HashMap<>();
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        Log.d(TAG, "onOpen:  conn = " + conn);
        mSocketClients.put(conn, conn.getRemoteSocketAddress().getAddress().getHostAddress());
        mCallBack.onOpen(handshake);
        mCallBack.updateSocketClient(mSocketClients);
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        Log.d(TAG, "onClose:  conn = " + conn);
        mSocketClients.remove(conn);
        mCallBack.updateSocketClient(mSocketClients);
    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer message) {
        super.onMessage(conn, message);
        mCallBack.onMessage(message);
        Log.d(TAG, "onMessage: ByteBuffer conn = " + conn);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        mCallBack.onMessage(message);
        Log.d(TAG, "onMessage: String conn = " + conn);
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        Log.d(TAG, "onError:  conn = " + conn + ", ex = " + ex.getMessage());

    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart: ");
    }

    public void send(List<String> SocketClient, String message) {
        for (String ipAddress : SocketClient) {
            for (Map.Entry<WebSocket, String> webSocketStringEntry : mSocketClients.entrySet()) {
                if (webSocketStringEntry.getValue().equals(ipAddress)) {
                    webSocketStringEntry.getKey().send(message);
                }
            }
        }
    }

    interface JWSCallBack {
        void onOpen(ClientHandshake handshake);

        void onMessage(String message);

        void onMessage(ByteBuffer bytes);

        void updateSocketClient(HashMap<WebSocket, String> socketHashMap);
    }
}
